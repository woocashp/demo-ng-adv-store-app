import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { CartStoreService } from '../../../containers/cart/services/cart.store.service';
import { AuthHttpStoreService } from '../../services/auth.store.service';
import { LoadingStateService } from '../../../shared/services/loading.store.service';
const { version } = require('../../../../../package.json');

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  version = version;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  cartCount$: Observable<number> = this.cartStore.getState().pipe(map((arr) => arr.reduce((acc, item: any) => acc + item.count, 0)));;
  loading$: Observable<boolean> = this.loadingStore.getState();
  auth$: Observable<boolean> = this.authStore.getState();

  constructor(
    private breakpointObserver: BreakpointObserver,
    private loadingStore: LoadingStateService,
    private cartStore: CartStoreService,
    private authStore: AuthHttpStoreService
  ) { }

  logOut() {
    this.authStore.logout();
  }

  ngOnInit(): void {
    this.authStore.isLogged();
  }

}
