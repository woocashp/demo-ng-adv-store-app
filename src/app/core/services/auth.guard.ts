import { NotificationService } from './../../shared/services/notification.service';
import { Injectable } from '@angular/core';
import { CanActivateChild } from '@angular/router';
import { Observable, iif, of, throwError } from 'rxjs';
import { retryWhen, delay, take, switchMap, catchError, timeout } from 'rxjs/operators';
import { AuthHttpStoreService } from './auth.store.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivateChild {

  constructor(
    private authStore: AuthHttpStoreService,
    private notification: NotificationService
  ) { }

  canActivateChild(): Observable<boolean> {
    return this.authStore.getState().pipe(
      switchMap((val) => iif(() => val, of(true), throwError(() => 'auth problem'))),
      retryWhen(err => err.pipe(delay(500), take(4))),
      timeout(2000),
      catchError((err) => {
        this.notification.showError('you shall not pass; try login');
        return throwError(() => err);
      })
    );
  }

}
