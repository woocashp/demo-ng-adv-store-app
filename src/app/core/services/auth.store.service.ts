import { tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpStore, HttpStoreConfig } from './http-store-service';
import { Injectable } from '@angular/core';
import { AuthDataModel, AuthResponse } from '../../utils/models';
import { Router } from '@angular/router';

@HttpStoreConfig({ url: 'https://auth.debugger.pl' })
@Injectable({ providedIn: 'root' })
export class AuthHttpStoreService extends HttpStore<boolean> {
  constructor(
    private router: Router,
    http: HttpClient
  ) {
    super(false, http);
  }

  isLogged() {
    const authorization = localStorage.getItem('token') || '';
    const headers = new HttpHeaders({ authorization });
    authorization && this.get('is-logged', headers, (resp: AuthResponse) => !resp.error);
  }

  login(data: AuthDataModel) {
    this.post('login', data, () => true)
      .pipe(tap((resp: any) => localStorage.setItem('token', resp.accessToken)))
      .subscribe();
  }

  logout() {
    this.setState(false);
    localStorage.removeItem('token');
    this.router.navigateByUrl('/');
  }
}
