import { mapTo, tap } from 'rxjs/operators';
import { CartStoreService } from './../../containers/cart/services/cart.store.service';
import { CartStorageService } from './../../containers/cart/services/cart.api.ls.service';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageGuard implements CanActivate {

  constructor(
    private cartStorage: CartStorageService,
    private cartStore: CartStoreService
  ) { }

  canActivate(): Observable<boolean> {
    return from(this.cartStorage.get())
      .pipe(
        tap((data = []) => this.cartStore.setState(data)),
        mapTo(true)
      )
  }

}
