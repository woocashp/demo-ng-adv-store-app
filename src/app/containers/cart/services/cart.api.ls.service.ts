import { Injectable } from '@angular/core';
import { CartItemModel } from '../../../utils/models';

export const LOCALSTORAGE_KEY = 'NG_ADV_APP_CART';

@Injectable({
  providedIn: 'root'
})
export class CartStorageService {

  get(): Promise<CartItemModel[]> {
    return Promise.resolve(JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY) || ''));
  }

  update(state: any): Promise<any> {
    localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(state));
    return Promise.resolve(true);
  }
}
