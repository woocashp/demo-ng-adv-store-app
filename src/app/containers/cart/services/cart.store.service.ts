import { ActionModel, CartTypes } from './../../../utils/models';
import { Injectable } from '@angular/core';
import { CartItemModel } from '../../../utils/models';
import { of } from 'rxjs';
import { skip, withLatestFrom, map } from 'rxjs/operators';
import { Utils } from '../../../utils/utils';
import { CartStorageService } from './cart.api.ls.service';
import { LoadingStateService } from '../../../shared/services/loading.store.service';
import { Store } from '../../../core/services/store';

@Injectable({
  providedIn: 'root'
})
export class CartStoreService extends Store<CartItemModel[]> {

  constructor(
    private loadingStore: LoadingStateService,
    private cartStorage: CartStorageService
  ) {
    super([]);
    this.getState()
      .pipe(skip(1))
      .subscribe((state) => {
        this.loadingStore.setState(true);
        this.cartStorage.update(state).then(() => this.loadingStore.setState(false))
      });
  }

  dispatch({ payload, type }: ActionModel<CartTypes>) {
    of(payload)
      .pipe(
        withLatestFrom(this.getState()),
        map(([item, state]: any) => type === 'increase'
          ? Utils.addOrIncreaseParam(state, item)
          : type === 'decrease'
            ? Utils.removeOrDecreaseParam(state, item)
            : Utils.remove(state, item)
        )
      )
      .subscribe((state) => this.setState(state))
  }
}
