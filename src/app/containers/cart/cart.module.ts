import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart/cart.component';
import { RouterModule } from '@angular/router';
import { DataGridModule } from 'demo-ng-kolekto';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [CartComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: CartComponent }
    ]),
    DataGridModule.forRoot({ bgColor: '#F8F8FF' }),
    MatIconModule,
    MatBadgeModule,
    MatButtonModule
  ]
})
export class CartModule { }
