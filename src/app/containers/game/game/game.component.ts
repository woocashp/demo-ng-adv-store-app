import { Message } from './../../../utils/models';
import { Component, ViewChild, TemplateRef, ViewContainerRef, ElementRef, AfterViewInit } from '@angular/core';
import { GameApiService } from '../services/game.api.service';
import { of, throwError, Observable, fromEvent, timer, merge } from 'rxjs';
import { throttleTime, map, switchMap, catchError, retry, scan, filter, take, groupBy, mergeMap, tap } from 'rxjs/operators';
import { LoadingStateService } from '../../../shared/services/loading.store.service';
import { EmbeddedViewRef } from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements AfterViewInit {

  @ViewChild('playerTpl') playerTpl!: TemplateRef<{ $implicit: Message }>;
  @ViewChild('area') area!: ElementRef;

  players: Map<string, EmbeddedViewRef<{ $implicit: Message }>> = new Map();
  stats$!: Observable<{ [k: string]: boolean }>;

  constructor(
    private gameApi: GameApiService,
    private container: ViewContainerRef,
    private loadingStore: LoadingStateService
  ) { }

  ngAfterViewInit(): void {
    this.loadingStore.setState(true);
    this.gameApi.getUser()
      .pipe(
        catchError(err => {
          this.loadingStore.setState(false);
          this.register();
          return throwError(() => err);
        })
      )
      .subscribe(() => {
        this.loadingStore.setState(false);
        this.init();
      })

    merge(
      fromEvent<MouseEvent | TouchEvent>(this.area.nativeElement, 'mousemove'),
      fromEvent<MouseEvent | TouchEvent>(this.area.nativeElement, 'touchmove')
    )
      .pipe(
        throttleTime(30)
      )
      .subscribe((e: MouseEvent | TouchEvent) => {
        let clientX, clientY;
        if (e instanceof TouchEvent) {
          clientX = e.touches[0].clientX;
          clientY = e.touches[0].clientY;
        } else {
          clientX = e.clientX;
          clientY = e.clientY;
        }
        this.gameApi.messanger.next({ clientX, clientY });
      })
  }

  updateView(msg: Message) {
    const playerExists = this.players.get(msg.username as string);
    if (playerExists)
      playerExists.context.$implicit = msg;
    else {
      const player = this.container.createEmbeddedView(this.playerTpl, { $implicit: msg });
      msg.username && this.players.set(msg.username, player);
    }
  }

  init() {
    this.stats$ = this.gameApi.messanger
      .pipe(
        tap((m) => this.updateView(m)),
        filter((msg: Message) => msg.username !== 'gift'),
        groupBy(({ username }) => username),
        mergeMap((g) => g.pipe(
          switchMap(({ username, type }) =>
            timer(0, 1000).pipe(
              take(2),
              map((val) => ({ username, active: !val, type }))
            ))
        )),
        scan((acc: any, msg: any) => {
          const result = { ...acc, [msg.username as string]: msg.active }
          msg.type === 'remove' && delete result[msg.username];
          return result;
        }, {})
      )
  }

  register() {
    of('your name /^[a-zA-Z]{3,6}$/')
      .pipe(
        map(txt => prompt(txt)),
        switchMap(username => this.gameApi.register(username as string)),
        catchError((error) => {
          console.log('register error', JSON.stringify(error));
          return throwError(() => error);
        }),
        retry(1)
      )
      .subscribe(this.init.bind(this))
  }

}
