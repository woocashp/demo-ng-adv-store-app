import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './game/game.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from "../../shared/shared.module";
@NgModule({
  declarations: [GameComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: GameComponent }
    ]),
    SharedModule
  ]
})
export class GameModule { }
