import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './components/register/register.component';
import { RouterModule } from '@angular/router';
import { FormModule, FORM_BG_COLOR } from 'demo-ng-kolekto';

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: RegisterComponent }
    ]),
    FormModule
  ],
  providers:[
    { provide: FORM_BG_COLOR, useValue: '#ADD8E6'}
  ]
})
export class RegisterModule { }
