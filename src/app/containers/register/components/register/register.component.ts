import { FieldConfig, FormEvents, FormValue } from 'demo-ng-kolekto';
import { Component, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Api } from '../../../../utils/api';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements AfterViewInit {
  formConfig$!: Observable<FieldConfig[]>;
  errors!: any[] | null | undefined;

  constructor(private http: HttpClient) { }
  ngAfterViewInit(): void {
    this.formConfig$ = this.http
      .get<{ data: FieldConfig[] }>(Api.DATA_FORM_CONFIG)
      .pipe(map((resp: { data: FieldConfig[] }) => resp.data));
  }

  onAction({ type, payload, errors }: FormValue) {
    this.errors = errors;
    if (type === FormEvents.submit && !errors?.length) alert(JSON.stringify(payload, null, 4));
  }
}
