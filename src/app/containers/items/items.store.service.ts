import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ItemModel } from '../../utils/models';
import { CartStoreService } from '../cart/services/cart.store.service';
import { HttpStore, HttpStoreConfig } from '../../core/services/http-store-service';

@HttpStoreConfig({ url: 'https://api.debugger.pl' })
@Injectable({ providedIn: 'root' })
export class ItemsStoreService extends HttpStore<ItemModel[]>{

  constructor(
    http: HttpClient,
    private cartStore: CartStoreService
  ) {
    super([], http);
  }

  fetchItems() {
    this.get('items', null, (resp) => resp.data);
  }

  buy(item: ItemModel) {
    this.cartStore.dispatch({ type: 'increase', payload: item });
  }
}
