import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
  data$: any;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.data$ = this.route.data.pipe(map((val) => val.itemDetail));
  }

}
