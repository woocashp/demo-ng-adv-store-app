import { Component, OnInit } from '@angular/core';
import { ItemsStoreService } from '../items.store.service';
import { Observable } from 'rxjs';
import { ItemModel } from '../../../utils/models';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items$!: Observable<ItemModel[]>;

  constructor(private itemsStore: ItemsStoreService) { }

  ngOnInit(): void {
    this.items$ = this.itemsStore.getState();
  }

  buy(item: ItemModel) {
    this.itemsStore.buy(item);
  }
}
